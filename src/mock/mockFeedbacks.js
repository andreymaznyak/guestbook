import Feedback from '../models/Feedback';

const mockFeedbacks = [
    new Feedback({ createdAt: 1540755095260, name: 'test 5', text: 'Bla Bla Bla 1'}),
    new Feedback({ createdAt: 1540755094261, name: 'test 4', text: 'Bla Bla Bla 2'}),
    new Feedback({ createdAt: 1540755093262, name: 'test 3', text: 'Bla Bla Bla 3'}),
    new Feedback({ createdAt: 1540755092263, name: 'test 2', text: 'Bla Bla Bla 4'}),
    new Feedback({ createdAt: 1540755091264, name: 'test 1', text: 'Bla Bla Bla 5'}),
];

export default mockFeedbacks;