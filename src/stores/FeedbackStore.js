
import { action, computed, observable } from 'mobx';
import Feedback from '../models/Feedback';
import mockFeedbacks from '../mock/mockFeedbacks';

const DEFAULT_PAGE_SIZE = 2;

class FeedbackStore {
    @observable pageSize: number = DEFAULT_PAGE_SIZE;
    @observable currentPage: number = 1;
    @observable feedbackRegistry = observable.map();

    @computed get feedbacks() {
        return Array.from(this.feedbackRegistry.values())
                    .sort((l, r) => r.createdAt - l.createdAt);
    }

    @computed get itemCount() {
        return this.feedbacks.length; 
    }

    @computed get pagesCount() {
        return Math.ceil(this.feedbackRegistry.size / this.pageSize);
    }

    @computed get currentPageFeedbacks() {
        const startIndex = (this.currentPage - 1) * this.pageSize;
        const endIndex = this.currentPage * this.pageSize;
        return this.feedbacks.slice(startIndex, endIndex);
    }

    @action.bound
    setCurrentPage(currentPage: number) {
        if (currentPage > this.pagesCount && currentPage < 1) {
            throw new Error(`Out of page ${currentPage} should be
             less then ${this.pagesCount} and greater then 0`)
        }
        this.currentPage = currentPage;
    }

    @action.bound
    setFeedbacks(feedbacks: Feedback[]) {
        this.clearFeedbacks();
        feedbacks.forEach( feedback => 
            this.feedbackRegistry.set(feedback.createdAt, feedback));
        
    }

    @action.bound
    addFeedback(feedback: Feedback) {
        this.feedbackRegistry.set(feedback.createdAt, feedback);
    }

    @action.bound
    clearFeedbacks() {
        this.feedbackRegistry.clear();
    }
}

const feedbackStore = new FeedbackStore();

feedbackStore.setFeedbacks(mockFeedbacks);

export default feedbackStore;

export { FeedbackStore };