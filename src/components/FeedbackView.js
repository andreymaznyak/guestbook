import IFeedback from '../models/Feedback';
import React from 'react';
import { Card, CardTitle, CardText, CardSubtitle } from 'reactstrap';

const FeedbackView = ({ feedback: { createdAt, name, email, text } }) => (
     <Card body>
        <CardTitle>{name}</CardTitle>
        <CardSubtitle>{email}</CardSubtitle>
        <CardText>{text}</CardText>
        <CardText>
            <small className="text-muted">{new Date(createdAt).toLocaleString()}</small>
          </CardText>
      </Card>
);

export default FeedbackView;