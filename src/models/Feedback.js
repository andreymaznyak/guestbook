// @flow
export interface IFeedback {
    +createdAt: number;
    name: string;
    email: ?string;
    text: string;
}

class Feedback implements IFeedback {
    _createdAt: number; // timestamp should be unique number
    name: string;
    email: ?string;
    text: string;
    constructor({ name, email, text, createdAt}: IFeedback) {
        Object.assign(this, {
            _createdAt: createdAt,
            name,
            email,
            text,
        });
    }

    get createdAt() {
        return this._createdAt;
    }
}

export default Feedback;