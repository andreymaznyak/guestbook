// @flow
import DevTools from 'mobx-react-devtools';
import { observer, Provider } from 'mobx-react';
import React, { Component } from 'react';
import { Container } from 'reactstrap';
import feedbackStore from './stores/FeedbackStore';
import Feedbacks from './containers/Feedbacks';
import CreateFeedback from './containers/CreateFeedback.js';
const stores = {
  feedbackStore,
};
const App = () => (
  <Provider {...stores}>
    <Container>
      <h1 className="pt-4">Guestbook</h1>
      <CreateFeedback />
      <Feedbacks />
      <DevTools />
    </Container>
  </Provider>
);

export default App;
