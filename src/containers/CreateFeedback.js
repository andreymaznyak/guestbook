// @flow
import React from 'react';
import { observer, inject } from 'mobx-react';
import { autorun } from 'mobx';
import FeedbackPagination from '../components/FeedbackPagination';
import FeedbackPage from '../components/FeedbackPage';
import validatorjs from 'validatorjs';
import MobxReactForm from 'mobx-react-form';
import FeedbackForm from '../components/FeedbackForm';
import Feedback from '../models/Feedback';

@inject('feedbackStore')
@observer
export default class CreateFeedback extends React.Component<any> {
    constructor() {
        super();
        this.initForm();
    }
    render() {
        const { form } = this.state;
        return (
            <FeedbackForm form={form} />
        );
    }

    initForm() {
        const plugins = { dvr: validatorjs };

        const fields = [{
            name: 'name',
            label: 'Name',
            placeholder: 'Name',
            rules: 'required|string',
        },{
            name: 'email',
            label: 'Email',
            placeholder: 'Insert Email',
            rules: 'email|string',
        },  {
            name: 'text',
            label: 'Feedback',
            placeholder: 'Feedback',
            type: 'textarea',
            rules: 'required|string|between:1,250',
        }];

        const hooks = {
            onSuccess: (form, ev) => {
                const { addFeedback } = this.props.feedbackStore;
                const feedback = new Feedback({...form.values(), createdAt: Date.now()});
                addFeedback(feedback);
                autorun( () => form.clear(), { delay: 1 }); // move to next event loop
            },
        }

        const form = new MobxReactForm({ fields }, { plugins, hooks });
        this.state = { form }; 
    }
}