import React from 'react';
import { observer } from 'mobx-react';
import { Form, FormGroup, Label, Input, FormFeedback, FormText, Button } from 'reactstrap';

@observer
export default class FeedbackForm extends React.Component {
    render() {
        const { form, onSubmit } = this.props;
        const submitHandler = async ev => {
            ev.currentTarget.focus();
            form.onSubmit(ev);
        }
        return (
        <Form>
            {Array.from(form.fields.values()).map( field => {
                return (
                <FormGroup key={field.id}>
                    <Label for={field.id}>{field.label}</Label>
                    <Input {...field.bind()} invalid={!!field.error} type={field.type}/>
                    <FormFeedback>{field.error}</FormFeedback>
                </FormGroup>
                )
            })}

            <Button 
                color={form.isValid ? 'success' : ''} 
                type="submit" 
                onClick={submitHandler}
                disabled={form.submitting}
            >Save Feedback</Button>

            <p>{form.error}</p>
        </Form>
        )
    }
}
