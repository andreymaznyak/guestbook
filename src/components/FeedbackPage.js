import React from 'react';
import FeedbackView from './FeedbackView';
const FeedbackPage = ({ feedbacks }) => (
    <div>
    {feedbacks.map(
        feedback => (
        <div className="pb-4" key={feedback.createdAt}>
            <FeedbackView feedback={feedback} />
        </div>
        )
    )}
    </div>
);

export default FeedbackPage;