// @flow
import React from 'react';
import { observer, inject } from 'mobx-react';
import FeedbackPagination from '../components/FeedbackPagination';
import FeedbackPage from '../components/FeedbackPage';

@inject('feedbackStore')
@observer
export default class Feedbacks extends React.Component<any> {
    render() {
        const { currentPageFeedbacks, pagesCount, currentPage, setCurrentPage } = this.props.feedbackStore;
        return (
            <div>
                <FeedbackPagination 
                    pagesCount={pagesCount}
                    currentPage={currentPage}
                    setPageHandler={setCurrentPage} />
                <FeedbackPage feedbacks={currentPageFeedbacks} />
            </div>
        );
    }
}