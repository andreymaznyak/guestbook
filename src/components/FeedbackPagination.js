import React, { Component } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';

const FeedbackPagination = ({
    currentPage,
    pagesCount,
    setPageHandler,
}) => {
    const pages = Array.from(Array(pagesCount).keys());
    const setLastPage = () => currentPage > 1 ? setPageHandler(currentPage - 1) : '';
    const setNextPage = () => currentPage < pagesCount ? setPageHandler(currentPage + 1) : '';
    return (
    <Pagination size="lg" aria-label="Page navigation" listClassName="justify-content-center">
        <PaginationItem onClick={setLastPage} disabled={currentPage === 1}>
            <PaginationLink previous />
        </PaginationItem>
        {pages.map(number => {
            const pageNumber = number + 1;
            const setPage = ev => setPageHandler(pageNumber);
            return (
            <PaginationItem active={currentPage === pageNumber} key={pageNumber} onClick={setPage}>
                <PaginationLink>
                    {pageNumber}
                </PaginationLink>
            </PaginationItem>);
        }
        )}
        <PaginationItem onClick={setNextPage} disabled={currentPage === pagesCount}>
            <PaginationLink next />
        </PaginationItem>
    </Pagination>
)
};

export default FeedbackPagination;